start transaction;
insert into books (isbn, id_publisher, id_category, name, description, image_medium, quantity, publishing_date, page_number, price, image_small) values ('978-5-9925-0844-4', 1513, 965, 'My new book', 'Description for new book', 'medium.jpg', 8, '2015-01-01', 100, 100, 'small.jpg');
set @bid = LAST_INSERT_ID();
insert into authors_books
(id_author, id_book, `order`)
values (3341, @bid, 1);
select @bid;
commit;