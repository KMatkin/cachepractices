all: daemon

daemon:
	docker cp daemon/src/ cachepractices_daemon_1:/var/daemon/src/

new:
	./insert_book.sh

.PHONY: all daemon new
